from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from products.views import ProductInfoView,GetProductByGroup
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Products API')

urlpatterns = [
    path('', schema_view),
    path('products', ProductInfoView.as_view()),
    path('products/get_product_by_group_name', GetProductByGroup.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
