from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from products.models import ProductInfo
from products.serializers import ProductInfoSerializer,ProductInfoSerializer2


class ProductInfoView(APIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.model = ProductInfo
        self.serializer = ProductInfoSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer(*args, **kwargs)

    def get(self, request):
        product_id=request.GET.get("product_id",None)
        if product_id:
            try:
                products = self.model.objects.get(product_id=product_id)
                serializer = self.serializer(products)
                return Response(serializer.data, status=status.HTTP_200_OK)
            except self.model.DoesNotExist:
                response={"msg":"no such product","status":status.HTTP_404_NOT_FOUND,"data":{}}
                return Response(response, status=status.HTTP_200_OK)
        else:
            products = self.model.objects.all()
            serializer = self.serializer(products, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


    def put(self, request):
        product_id = request.data.get('product_id', None)
        product = get_object_or_404(self.model, product_id=product_id)
        serializer = self.serializer(product, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)

    def delete(self, request):
        product_id = request.data.get('product_id', None)
        product = get_object_or_404(self.model, product_id=product_id)
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class GetProductByGroup(APIView):
    serializer_class = ProductInfoSerializer2
    def get(self,request):
        group_name = request.GET.get("group_name", None)
        if group_name:
            try:
                products = ProductInfo.objects.filter(groups__contains=group_name)
                print(products)
                serializer = ProductInfoSerializer2(products,many=True)

                return Response(serializer.data, status=status.HTTP_200_OK)
            except ProductInfo.DoesNotExist:
                response={"msg":"no such product associated with group","status":status.HTTP_404_NOT_FOUND,"data":{}}
                return Response(response, status=status.HTTP_200_OK)
        else:
            response = {"msg": "please provide 'group_name' as query parameter", "status": status.HTTP_404_NOT_FOUND, "data": {}}
            return Response(response, status=status.HTTP_200_OK)
