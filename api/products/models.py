from django.db import models
from django.db.models import Max
from products import fields


class ProductInfo(models.Model):
    product_id = models.CharField(max_length=6, primary_key=True, editable=False)
    branch_category = models.CharField(max_length=50)
    product_type = models.CharField(max_length=100)
    product_name = models.CharField(max_length=100)
    description = models.TextField(max_length=1000, null=True)
    image = models.ImageField(upload_to='product-images', default='product-images/product.png')
    owner = models.CharField(max_length=50)
    ap_path = models.URLField()
    cp_path = models.URLField()
    groups = fields.ListField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def save(self, **kwargs):
        if not self.product_id:
            max_id = ProductInfo.objects.aggregate(id_max=Max('product_id'))['id_max']
            self.product_id = "{}{:04d}".format('PD', int(max_id[2:]) + 1 if max_id is not None else 1)
        super().save(**kwargs)
