from rest_framework import serializers
from products.models import ProductInfo

branch_categories = [
    ('Master', 'Master'), ('Child', 'Child'),
]


class ProductInfoSerializer(serializers.ModelSerializer):
    branch_category = serializers.ChoiceField(choices=branch_categories)
    groups = serializers.ListField(default=[])
    created_on = serializers.DateTimeField(read_only=True)
    modified_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = ProductInfo
        fields = '__all__'
class ProductInfoSerializer2(serializers.ModelSerializer):
    class Meta:
        model = ProductInfo
        fields = '__all__'
